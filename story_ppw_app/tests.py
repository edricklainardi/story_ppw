from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profil
from .models import Status
from .forms import HomeForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles import finders


import time


# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/story_ppw_app/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_url_profil_is_exist(self):
        response = Client().get('/story_ppw_app/profil/')
        self.assertEqual(response.status_code, 200)

    def test_profil_page_have_text(self):
        request = HttpRequest()
        response = profil(request)
        html = response.content.decode('utf8')
        self.assertIn('<title>Bio</title>', html)
        self.assertIn('Name', html)
        self.assertIn('Occupation', html)
        self.assertIn('Birthdate', html)
        self.assertIn('Hobby', html)
        self.assertIn('Motto', html)

    def test_lab6_using_index_func(self):
        found = resolve('/story_ppw_app/')
        self.assertEqual(found.func, index)

    def test_home_page_have_text(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        self.assertIn('<h1 align="center">Hello, apa kabar?</h1>', html)

    def test_model_check_items_in_model(self):
        Status.objects.create(status='test')
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_string_representation(self):
        status = 'status saya'
        coba = Status(status='status saya')
        self.assertEqual(status, coba.status)

    def test_form_validation_for_blank_items(self):
        form = HomeForm(data={'status': '',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ['This field is required.']
        )
    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/story_ppw_app/', {'status': test,})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/story_ppw_app/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/story_ppw_app/', {'status': '',})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/story_ppw_app/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    # def test_lab6_using_library_func(self):
    #     found = resolve('/story_ppw_app/library/data')
    #     self.assertEqual(found.func, library)
    #
    # def test_lab6_using_lib_func(self):
    #     found = resolve('/story_ppw_app/library')
    #     self.assertEqual(found.func, lib)

    def test_story6_dict_is_saved(self):
        response = Client().get('/story_ppw_app/library/data')
        self.assertEqual(response.status_code, 301)


class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

#    def test_input_todo(self):
#        selenium = self.selenium
#        selenium.get('https://story-ppw.herokuapp.com/story_ppw_app/')
#        status = selenium.find_element_by_name('status')
#        submit = selenium.find_element_by_id('submit')
#        status.send_keys('Coba Coba')
#        submit.send_keys(Keys.RETURN)
#        list = selenium.find_elements(By.XPATH, "//h3[.='Coba Coba']");
#        assert (len(list) > 0)
#        self.assertIn('Landing Page', selenium.title)
#        time.sleep(3)

#    def test_navbar_color_true(self):
#        selenium = self.selenium
#        selenium.get('https://story-ppw.herokuapp.com/story_ppw_app/')
#        navbar_color = selenium.find_element_by_tag_name('nav').get_attribute("color")
#        self.assertEqual('#efef51', navbar_color)

#    def test_css_dir_true(self):
#        self.assertTrue(staticfiles_storage.exists(finders.find('../static/css/style.css')))

#    def test_positioning_1(self):
#        selenium = self.selenium
#        selenium.get('https://story-ppw.herokuapp.com/story_ppw_app/')
#        inputbox = selenium.find_element_by_id('id_status')
#        selenium.set_window_size(1024, 768)
#        self.assertAlmostEqual(
#            inputbox.location['x'] + inputbox.size['width'] / 2,
#            512,
#            delta=15
#        )

#    def test_positioning_2(self):
#        selenium = self.selenium
#        selenium.get('https://story-ppw.herokuapp.com/story_ppw_app/')
#        text = selenium.find_element_by_tag_name('h1')
#        selenium.set_window_size(1024, 768)
#        self.assertAlmostEqual(
#            text.location['x'] + text.size['width'] / 2,
#            512,
#            delta=15
#        )
