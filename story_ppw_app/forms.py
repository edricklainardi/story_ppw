from django import forms
from story_ppw_app.models import Status, Register

class HomeForm(forms.ModelForm):
    status = forms.CharField(max_length=300)

    class Meta:
        model = Status
        fields = ('status',)

class RegistForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Name',
            'id' : 'name',
        }
    ))
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'type': 'email',
            'class': 'form-control',
            'placeholder': 'E-mail',
            'id' : 'email',
        }
    ))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'type': 'password',
            'class': 'form-control',
            'placeholder': 'Password',
            'id' : 'password',
        }
    ))

    class Meta:
        model = Register
        fields = ('name','email','password',)
