from django.db import models

class Status(models.Model):
    status = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now_add=True)

class Register(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    password = models.CharField(max_length=300)
