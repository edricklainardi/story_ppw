from django.apps import AppConfig


class StoryPpwAppConfig(AppConfig):
    name = 'story_ppw_app'
