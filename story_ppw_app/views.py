from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse
from story_ppw_app.forms import HomeForm, RegistForm
from story_ppw_app.models import Status, Register
from django.contrib import messages
import requests
import simplejson as json

def index(request):
    form = HomeForm(request.POST)
    statuses = Status.objects.all()
    if form.is_valid():
        status = form.save(commit=False)
        status.save()
        form = HomeForm()
        return HttpResponseRedirect(reverse('index'))
    return render(request, 'story_ppw_app/index.html', {'form': form, 'statuses' : statuses})

def profil(request):
    return render(request, 'story_ppw_app/profil.html', {})

def lib(request):
    return render(request, 'story_ppw_app/library.html', {})

def library(request):
    judul = request.GET.get('judul', 'quilting')
    search_results = search_book(judul)
    items = search_results
    return JsonResponse({'dataku':items})

def search_book(judul):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + judul
    req = requests.get(url).json()
    all_data = []
    get_datas = req['items']
    for data in get_datas:
        dict = {"title": data['volumeInfo']['title'],"authors": data['volumeInfo']['authors'],"publishedDate": data['volumeInfo']['publishedDate'],}
        all_data.append(dict)
    return all_data

def register(request):
    if request.method == 'POST':
        form = RegistForm(request.POST)
        if form.is_valid():
            name = request.POST['name']
            email = request.POST['email']
            password = request.POST['password']
            existing_user = Register.objects.filter(email=form.cleaned_data.get('email')).first()
            if existing_user:
                messages.info(request, 'E-mail has already been taken')
                return HttpResponseRedirect(reverse('register'))
            else:
                Register.objects.create(name=name , email=email, password=password)
                messages.info(request, 'User registered successfully')
                return HttpResponseRedirect(reverse('register'))
    else:
        form = RegistForm()
        return render(request, 'story_ppw_app/register.html', {'form': form})

def checkEmail(request):
    if request.method == "POST":
        email = request.POST['email']
        bool = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email': bool})

def post(self,request, *args, **kwargs):
    if self.request.is_ajax():
        return self.ajax(request)

def ajax(request):
    registerer = Register.objects.all()
    response_dict = {
        'name':registerer.name,
        'email':registerer.email
    }
    return HttpResponse(json.dumps(response_dict),
                        mimetype='application/json')
