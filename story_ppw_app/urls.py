from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('profil/', views.profil, name='profil'),
    path('library/', views.lib, name='library'),
    path('library/data/', views.library, name='data'),
    path('register/', views.register, name='register'),
    path('register/email', views.checkEmail, name='email'),
    path('register/email/list', views.ajax, name='list'),
]
