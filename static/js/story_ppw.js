$(document).ready(
    function() {
        document.getElementById('grayButton').onclick = switchGray;
        document.getElementById('whiteButton').onclick = switchWhite;
        document.getElementById('blueButton').onclick = switchBlue;
        document.getElementById('yellowButton').onclick = switchYellow;

    function switchGray() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'gray';
        document.getElementsByTagName('body')[0].style.color = 'white';
        document.getElementsByTagName('p')[3].style.color = 'black';
        document.getElementsByTagName('p')[4].style.color = 'black';
        document.getElementsByTagName('p')[2].style.color = 'black';
    }

    function switchWhite() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'white';
        document.getElementsByTagName('body')[0].style.color = 'black';
    }

    function switchBlue() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'blue';
        document.getElementsByTagName('body')[0].style.color = 'white';
        document.getElementsByTagName('p')[3].style.color = 'black';
        document.getElementsByTagName('p')[4].style.color = 'black';
        document.getElementsByTagName('p')[2].style.color = 'black';
    }

    function switchYellow() {
        document.getElementsByTagName('body')[0].style.backgroundColor = 'yellow';
        document.getElementsByTagName('body')[0].style.color = 'black';
    }

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
});

// $(document).ready(
//     function() {
//         $.ajax({
//             type: "json",
//             method: "GET",
//             url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
//             success: function(data) {
//                 console.log(data);
//                 var books = data.items;
//                 for (var i = 0; i < data.length; i++) {
//                     var tmp = "<tr><td>" + books[i].title + "</td><td>" + books[i].authors + "</td><td>" + books[i].publishedDate + "</td></tr>";
//                     $("#library").append(tmp);
//                 }
//             }
//         })
//     }
// );

$(document).ready(function () {
            $.ajax({
                method: "GET",
                url: "data/",
                success: function (datanya) {
                    var data = datanya.dataku;
                    for (var i = 0; i < data.length; i++) {
                        var thisData = data[i];
                        var html = '<tr><td><button id="btn"><i class="btn"></i></button><button id="btn2" style="visibility:hidden"><i class="btn2"></i></button>' + thisData.title + '</td><td>' + thisData.authors + '</td><td>' + thisData.publishedDate + '</td></tr>';
                        $('tbody').append(html);
                    }
                }
            })
        });

        $(".click").click(
            function() {
                console.log("hello");
                $.ajax({
                    method: "GET",
                    url: "data/",
                    data: {judul : $(this).attr('value'),},
                    success: function (datanya) {
                        var data = datanya.dataku;
                        for (var i = 0; i < data.length; i++) {
                            var thisData = data[i];
                            var html = '<tr><td><button id="btn"><i class="btn"></i></button><button id="btn2" style="visibility:hidden"><i class="btn2"></i></button>' + thisData.title + '</td><td>' + thisData.authors + '</td><td>' + thisData.publishedDate + '</td></tr>';
                            $('tbody').append(html);
                        }
                    }
                })
            });

$(document).ready(
    function() {
        var counter = 0;
        document.getElementById('btn').onclick = increment;
        document.getElementById('btn2').onclick = decrement;
    function increment() {
        counter += 1;
        document.getElementById("counter").innerHTML = counter;
        document.getElementById("btn").style.visibility = 'hidden';
        document.getElementById("btn2").style.visibility = 'visible';
    }
    function decrement() {
        counter -= 1;
        document.getElementById("counter").innerHTML = counter;
        document.getElementById("btn2").style.visibility = 'hidden';
        document.getElementById("btn").style.visibility = 'visible';
    }
});

$(document).ready(
    function () {
        $('#submit').prop('disabled', true);
        var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/register/email",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) {
                var inptEmail = $("#email");
                if (response.is_email) {
                    $("#submit").prop('disabled', false);
                } else {
                    $("#submit").prop('disabled', true);
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    $(function () {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $('form').on('submit', function (e) {
          e.preventDefault();
          $.ajax({
            method: "POST",
            url: '/register/',
            headers:{
                "X-CSRFToken": csrftoken
            },
            success: function () {
                $("#submit").prop('disabled', true);
            },
            error: function(error) {
                alert("Error, cannot connect to server")
            }
          });
        });
      });
});

$(document).ready(function () {
    $(document).on("click",'#submit', function() {
        $.ajax({
            type: "POST",
            // This is the dictionary you are SENDING to your Django code.
            // We are sending the 'action':add_car and the 'id: $car_id
            // which is a variable that contains what car the user selected
            data: { action: "add_id" },
            success: function(data){
                // This will execute when where Django code returns a dictionary
                // called 'data' back to us.
                $("#id").html("<strong>"+data.name+ " " +data.email+"</strong>");
            }
        });
    });
});
